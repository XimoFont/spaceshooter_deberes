﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet5 : Bullet {
    public int maxAmmo = 10;
    Cartridge cartridge;
    public int timeToExplode = 1;
    private float currentTime = 0f;

    void Awake()
    {
        cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[1];
    }

    protected new virtual void Update()
    {
        base.Update();
        if (shooting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > timeToExplode*0.5f)
            {
                ShotMiniBullets();
            }
        }
        else
        {
            currentTime = 0f;
        }
    }

    void ShotMiniBullets()
    {
        Debug.Log("Shot mini bullets");
        float z = transform.rotation.eulerAngles.z;
        cartridge.GetBullet().Shot(transform.position, -90 + z);
        cartridge.GetBullet().Shot(transform.position, 90 + z);
        cartridge.GetBullet().Shot(transform.position, 0 + z);
        Reset();
    }
}