﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceManager : MonoBehaviour {

    public static SpaceManager instance;
    private int highscore;
    private int lifeplayer;
    public Text highscoreText;
    public Text lifeText;
    public GameObject GameOver;
    public GameObject Pause;



	// Use this for initialization
	void Start () {
        instance = this;
        highscore = 0;
        lifeplayer = 5;
        lifeText.text = "x" + lifeplayer.ToString();
        highscoreText.text = highscore.ToString("D5");

    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Pauseg();

        }
    
    }



    public void addHighscore(int value)
    {
        highscore += value;
        highscoreText.text = highscore.ToString("D5");
    }

    public void sublive()
    {
        if(lifeplayer > 0)
        {
            lifeplayer--;
            lifeText.text = "X" + lifeplayer.ToString();     
        }
        else
        {
            GameOverg();
        
        }
    }

    public void Pauseg()
    {
        Pause.SetActive(!Pause.active);
        if(Pause.active == true)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }
    public void GameOverg()
    {
        GameOver.SetActive(!GameOver.active);
        if(GameOver.active == true)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }

}
